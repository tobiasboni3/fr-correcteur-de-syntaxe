import corrector  # Part of the software which correct french syntax
import tkinterGUI as gui  # Part of the software which draw / create graphical interface

try:
    while True:
        string_print = gui.string.get()
        gui.dynamic.config(text=corrector.correction(string_print))
        gui.dynamic_label_words.config(text=corrector.number_of_words(corrector.correction(string_print)))
        gui.dynamic_label_sentences.config(text=corrector.number_of_sentences(corrector.correction(string_print)))
        gui.dynamic_label_containers.config(text=corrector.number_of_containers(corrector.correction(string_print)))
        gui.window.update()
        if string_print == 'exit':
            break

except Exception as E:
    print('Stop destroy my software !  \n', E)