def capitalize(var):
    """
    First we make everything lowercase, then add an uppercase
    """
    var = var.lower()
    var = var[0].upper() + var[1::]
    for i in range(len(var) - 4):
        if var[i] in [".", "!", "?"]:
            var = var[0:i + 2:] + var[i + 2].upper() + var[i + 3::]
    return var


def end_punctuation(var):
    """
    add end sentence punctuation
    """
    if var[-1] == '!':
        var = var[:-1]
        var = var + ' ' + '!'
    elif var[-1] == '?':
        var = var[:-1]
        var = var + ' ' + '?'
    elif var[-1] in (',', ';', ':'):
        var = var[:-1]
        var = var + '.'
    elif var[-1] != '.':
        var = var + '.'
    return var


def comma_or_excess_dot(var):
    """
    This function delete excess point or dot, and this function replace ,. and .,
    """
    while ".." in var:
        var = var.replace('..', '.', var.count('..'))
    var = var.replace(',.', ',').replace('.,', ',')
    while ",," in var:
        var = var.replace(',,', ',')
    return var


def destroy_space(var):
    """
    Simply : Just delete extra-space and space before commas and dots
    """
    var = ' '.join(var.split()).replace(' ,', ',').replace(' .', '.')
    return var


def add_a_space_after_comma(var):
    """
    Replace punctuation to punctuation with a space AHAHAHAHAHAHHAHAHAHAHAH GENIUS
    But don't execute them if we don't have punctuation (if*5 > alwails loop)
    """
    var = var.replace('.', '. ', var.count('.'))
    if ',' in var:
        var = var.replace(',', ', ', var.count(','))
    if '?' in var:
        var = var.replace('?', ' ? ', var.count('?'))
    if '!' in var:
        var = var.replace('!', ' ! ', var.count('!'))
    if ';' in var:
        var = var.replace(';', '; ', var.count(';'))
    if ':' in var:
        var = var.replace(':', ': ', var.count(':'))
    return var


# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
def correction(var):
    """
    Main Function which call all the others
    """
    if len(var) > 0:  # Start only if the user input something
        return capitalize(end_punctuation(comma_or_excess_dot(destroy_space(add_a_space_after_comma(var)))))
    else:  # else, ask the user to input something
        return "Input a sentence, or tap 'exit' to quit"


# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////


def number_of_words(var):
    """
    count the number of words
    """
    var = len(var.split()) - var.count('!') - var.count('?')
    if var == 8:
        return 'Sentences = None'
    var = 'Words = ' + str(var)
    return var


def number_of_sentences(var):
    """
    count the number of sentences (in reality count the number of dot)
    """
    var = var.count('.') + var.count('!') + var.count('?')
    if var == 0:
        return 'Sentences = None'
    var = 'Sentences = ' + str(var)
    return var


def number_of_containers(var):
    """
    count the number of containers (in reality count the number of punctuation)
    """
    var = var.count(',') + var.count('.') + var.count('?') + var.count('!') + var.count(':') + var.count(';') - 1
    if var == -1:
        return 'Containers = None'
    var = 'Containers = ' + str(var)
    return var
