from tkinter import Tk, Label, Entry, StringVar, BOTH, LEFT

window = Tk()
window.title('Syntax Corrector')
window.geometry('1024x320')

label_input = Label(window, text='Input a sentence : ', font='Arial 16')
label_input.pack(fill=BOTH, expand=1)
string = Entry(window, textvariable=StringVar, width=127, font='Arial 16', fg='blue')
string.pack(fill=BOTH, expand=1)
space = Label(window, text='Live Corrections : ', font='Arial 16',)
space.pack(fill=BOTH, expand=1)
dynamic = Label(window, textvariable=string.get(), font='Arial 16', bg='white', fg='red')
dynamic.pack(fill=BOTH, expand=1,)

dynamic_label_words = Label(window, font='Arial 13', fg='green')
dynamic_label_words.pack(side=LEFT, expand=0.1)
dynamic_label_sentences = Label(window, font='Arial 13', fg='orange')
dynamic_label_sentences.pack(side=LEFT, expand=0.1)
dynamic_label_containers = Label(window, font='Arial 13', fg='purple')
dynamic_label_containers.pack(side=LEFT, expand=0.1)